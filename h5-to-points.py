#!/usr/bin/env python
# -*- coding: utf-8 -*-

import h5py, sys, os
from utils import sizeof_fmt
from pointConverter import savePointsAsXYZ, savePointsAsPdb
from os.path import basename
from os.path import splitext

def h5_points_convert(h5_fileName, levels=['level3']):
    """Converts chromatine models from in h5 file into pointsdata structure.
        Single chromosome in file only!

        Args:
            h5_filename (str): filename
        Returns:
            points (list of three floats tuples)
    """
    hf = h5py.File(h5_fileName, 'r', libver='latest')
    size = os.stat(h5_fileName).st_size
    print 'File {} ( {} ) opened...'.format(h5_fileName, sizeof_fmt(size))
    chromosome = hf['HCMConverted'].keys()[0]
    if len(hf['HCMConverted'].keys()) > 1:
        print "[ WARN ] More than one chromosome found. Only first will be converted!"
    print "Model of {} found...".format(chromosome)
    levels_in_h5 = [i for i in hf['/HCMConverted/{}/'.format(chromosome)].keys() if 'level' in i]

    # Print sizes
    for level in levels_in_h5:    # 0-chromosom, 1-segments, 2-anchors/subanchors, 3-spline
        print 'Level {} contains {} beads.'.format(level[-1],
                                                   hf['/HCMConverted/{}/{}/coordinates'.format(chromosome,
                                                                                               level)].shape[0])

    for level in levels:    # 0-chromosom, 1-segments, 2-anchors/subanchors, 3-spline
        points = []
        for i in hf['/HCMConverted/{}/{}/coordinates'.format(chromosome, level)][:]:
            points.append(tuple(i))

        if len(points) > 0:
#            outFileName = '{}_lvl{}.xyz'.format(splitext(basename(h5_fileName))[0], level[-1])
#            savePointsAsXYZ(points, outFileName, fmt='chimera', molecule_name='{} lvl {}'.format(chromosome, level[-1]) ) 
            outFileName = '{}_lvl{}.pdb'.format(splitext(basename(h5_fileName))[0], level[-1])
            savePointsAsPdb(points, outFileName) 
    hf.close()

    
    


def main():
    h5_points_convert(sys.argv[1])    

if __name__ == '__main__':
    main()
