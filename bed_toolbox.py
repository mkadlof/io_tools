#!/usr/bin/env python
# -*- coding: utf-8 -*-

class BEDrecord:
    def __init__(self, string):
        string = string.split()
        self.chr = string[0]
        self.begin = int(string[1])
        self.end = int(string[5])
        self.kompartment = string[3]

    def __len__(self):
        return self.end-self.begin

    def __str__(self):
        return '{}\t{}\t{}\t{}\n'.format(self.chr, self.begin, self.end, self.kompartment)

def main():
    pass

if __name__ == '__main__':
    main()
