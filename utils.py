#!/usr/bin/env python
# -*- coding: utf-8 -*-

def read_and_report_np_array(fname, skiprows=0):
    """Read and prints some basic informations about numpy array."""
    import os
    import numpy as np
    size = os.stat(fname).st_size
    print "Reading {} file ({})...".format(fname, sizeof_fmt(size))
    matrix = np.loadtxt(fname, skiprows=skiprows)
    print "Array of shape {} loaded.".format(matrix.shape)
    print "Min: {} Max: {}".format(np.amin(matrix), np.amax(matrix))
    return matrix

def sizeof_fmt(num, suffix='B'):
    """Returns human readable size format (using binary prefixes)
    
        Args:
            num (float) : number to size
            suffix (str): a unit
    """
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f %s%s" % (num, 'Yi', suffix)

def file_size(path):
    """Returns human radeble string with file size"""
    import os
    size = os.stat(path).st_size
    return sizeof_fmt(size)

def write_chimera_atribute_file(attributes, attribute_name, out_file_name):
    """Writes cells attributes in chimera atribute file format.

    Args:
        atributes (list of string): vector of atributes
        atribute_name (string): single word that describe the attribute in chimera
        out_file_name (str): If empty, file extension will be replaced with .chimera.atr
    """
    out_file = open(out_file_name, 'w')
    out_file.write('attribute: {}\n'.format(attribute_name))
    out_file.write('match mode: 1-to-1\n')
    out_file.write('recipient: atoms\n')
    for k, i in enumerate(attributes):
        #out_file.write("\t:1@c{}\t{}\n".format(k+1, i))
        out_file.write("\t:{}\t{}\n".format(k+1, i))
    out_file.close()
    print "File {} in Chimera attributes format saved...".format(out_file_name)


def main():
    pass

if __name__ == '__main__':
    main()
