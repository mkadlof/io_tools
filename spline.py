#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
functions to perform spline interpolation
"""

from scipy import interpolate
import numpy as np
from distance import distance3d as d
from distance import point_between

def spline(points, ref_dist=1.0):
    """Performs spline interpolation out of list of points

    Args:
        points (list of three floats tuples) : points that will be used as spline nodes
        ref_dist (float) : desired distances between beads 

    Returns:
        points (list of three floats tuples) : points after interpolation
    """
    points = np.asarray(points)
    points = points.transpose()
    tck, u = interpolate.splprep(points, s=0)
    new = interpolate.splev(np.linspace(0, 1, 50000000), tck) # ta duża liczba jest ważna. Im większa, tym lepsza precyzja. i tym dłuzej sie liczy.
    bla = zip(new[0], new[1], new[2])
    p = [bla[0]]
    n = len(bla)
    previousPoint = bla[0]
    distanceToLastBead = 0
    for i in xrange(n):        
        currentPoint = bla[i]
        loc_dist = d(previousPoint, currentPoint)
        distanceToLastBead += loc_dist
        if distanceToLastBead > ref_dist:
            proportion = (distanceToLastBead - ref_dist)/loc_dist
            p.append(point_between(previousPoint, currentPoint, prop=proportion))
            distanceToLastBead = distanceToLastBead - ref_dist
        previousPoint = currentPoint
    return p
    
def _main():
    print d([0, 0, 0], [3, 4, 0])
    print d([0, 0, 0], [1, 1, 1])
#    points = [[0,0,0], [10,0,0], [10,10,0], [10,10,-10], [10,0,-10], [0,0,-10], [0,10,-10], [0,10,0]]
#    spline(points)

if __name__ == '__main__':
    _main()

