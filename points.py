#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
from tools.distance import distance3d as d

points = [(1.1, 2.3, 3.4),
          (0.1, 5.3, 4.2),
          (9.2, 4.3, 1.3)]

def diameter(points):
    """find the maximal distance in points"""
    dia = 0
    for i in points:
        for j in points:
            dist = d(i,j)
            if dist > dia:
                dia = dist
    return dia

def main():
    print diameter(points)

if __name__ == '__main__':
    main()
